﻿Shader "Unlit/ScreenUvAnimation"
{
	Properties
	{
		[HDR]_Color("Color",color) = (1,1,1,1)
		[HDR]_HitColor("HitCOlor",color) = (1,1,1,1)
		_Pow("Pow",range(0,10)) = 3
		_Alpha("Alpha",range(0,1)) = 0
		_MainTex("MainTex",2D) = "black"{}
		_UvScale("UVscale",range(5,128)) = 5
		_LodMips("LodMips",range(0,1)) = 0
    }
    SubShader
    {
        Tags { "queue"="Geometry+1" }
        LOD 100

			pass
			{
				//offset  
					cull front
					blend srcalpha oneminussrcalpha
					//zwrite off
					//zTest Always
					CGPROGRAM
					#pragma vertex vert
					#pragma fragment frag

					#include "UnityCG.cginc"
						
					struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 n:NORMAL;
					fixed4 color : COLOR;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};
				v2f vert(appdata v)
				{
					v2f o;
					//o.vertex = UnityObjectToClipPos(v.vertex);
					float3 vNormal = UnityObjectToWorldNormal(v.n);
					vNormal = mul(UNITY_MATRIX_V, vNormal);
					float4 vPos = mul(UNITY_MATRIX_MV, v.vertex);
					vPos.xyz += vNormal * 0.003f;
					o.vertex = mul(UNITY_MATRIX_P, vPos);
					return o;
				}
				fixed4 _HitColor;
				float _Alpha;
				fixed4 frag(v2f i) : SV_Target
				{
					_HitColor.a *= _Alpha;
					return _HitColor;
				}
					ENDCG
			}
    }
}
