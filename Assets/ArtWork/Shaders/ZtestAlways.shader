﻿Shader "Unlit/ZtestAlways"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color",color)=(0.5,0.5,0.5,0.5)
		_Range("Range",range(0,1)) = 0.272
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 100

        Pass
        {
			blend srcalpha oneminussrcalpha
			zwrite off
			ztest always
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _Range;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
                return o;
            }
			fixed4 _Color;
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				col.rgb = i.color.rgb;
				//col.a *= i.color.a;
				fixed4 fc = saturate(fixed4(col.rgb, col.a-((1-i.color.a)/3)- _Range));
                return fc;
            }
            ENDCG
        }
    }
}
