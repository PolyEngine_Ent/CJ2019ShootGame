﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine.UnityNetworking;


namespace Ximmerse.RhinoX.Experimental
{
    /// <summary>
    /// Scripting API base class.
    /// </summary>
    internal abstract class ScriptingAPI : MonoBehaviour
    {
        public ARCamera arCamera
        {
            get
            {
                return ARCamera.Instance;
            }
        }

        protected bool IsAndroid
        {
            get
            {
                return Application.platform == RuntimePlatform.Android;
            }
        }

        static internal List<ScriptingAPI> apis = new List<ScriptingAPI>();

        protected virtual void Awake()
        {
            apis.Add(this);
        }

        /// <summary>
        /// Invokes API RPC
        /// </summary>
        /// <param name="RpcMethodName">Rpc method name.</param>
        protected void InvokeRpc (string RpcMethodName)
        {
            NetworkRPC.InvokeRpc(this, RpcMethodName, null);
        }

        /// <summary>
        /// Invokes API RPC
        /// </summary>
        /// <param name="RpcMethodName">Rpc method name.</param>
        protected void InvokeRpc(string RpcMethodName, object[] parameters)
        {
            NetworkRPC.InvokeRpc(this, RpcMethodName, parameters);
        }
    }
}