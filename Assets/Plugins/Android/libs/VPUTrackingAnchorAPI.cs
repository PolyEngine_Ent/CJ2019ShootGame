﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine.Reflection;
using PolyEngine;
using System.Reflection;

namespace Ximmerse.RhinoX.Experimental
{
    /// <summary>
    /// API : manipulate 3D eye view frustum,
    /// </summary>
    internal class VPUTrackingAnchorAPI : ScriptingAPI
    {
        [Header("--- VPU Anchor ---")]

        [SerializeField]
        Vector3 m_AnchorOffset = new Vector3(-0.0238f, 0.0809f, 0.1412f);

        [SyncProperty]
        public Vector3 AnchorOffset
        {
            get
            {
                return m_AnchorOffset;
            }
            set
            {
                m_AnchorOffset = value;
                UpdateVPUAnchorTransform();
            }
        }

        [SerializeField]
        Vector3 m_AnchorTilt = new Vector3(35, 0, 0);

        [SyncProperty]
        public Vector3 AnchorTilt
        {
            get
            {
                return m_AnchorTilt;
            }
            set
            {
                m_AnchorTilt = value;
                UpdateVPUAnchorTransform();
            }
        }

        Transform trackingAnchor = null;

        IEnumerator Start ()
        {
            if (IsAndroid == false)
                yield break;
            while (!arCamera.IsARBegan)
            {
                yield return null;
            }
            trackingAnchor = arCamera.GetFieldValue<Transform>("trackingAnchor", BindingFlags.Instance | BindingFlags.NonPublic);

            AnchorOffset = AnchorOffset;
            AnchorTilt = AnchorTilt;
        }

        private void UpdateVPUAnchorTransform ()
        {
            if (IsAndroid == false)
                return;
            trackingAnchor.localPosition = m_AnchorOffset;
            trackingAnchor.localEulerAngles = m_AnchorTilt;
        }
    }
}