﻿using System;
using UnityEngine;

namespace Ximmerse.RhinoX.Experimental
{
    [AttributeUsage (AttributeTargets.Property)]
    internal class SyncPropertyAttribute : System.Attribute
    {

    }

    [AttributeUsage(AttributeTargets.Method)]
    internal class SyncRPCAttribute : PropertyAttribute
    {

    }
}