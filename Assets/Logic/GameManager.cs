﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine;
using System;

public enum GameState
{
    PreRedy,
    Start,
    Puse,
    Over
}

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public GameObject globleUI;

    public string readyWords;

    public int frameRate = 120;
    public int fireCount = 0;

    public int playerScore;

    [Header("中靶数量\n")]
    [SerializeField] int targetPassCount = 0;

    public float amoutTime = 0;
    public float costTime = 0;
    public float overloadTime=0;

    public static float gameTime;
    public float gameTimeRest;
    //加分
    public Action<int> ScoreProcess;

    //每次游戏循环会回到Over，以便倒计时再次开始游戏
    public GameState gameState = GameState.Over;

    public bool isDQ = false;

    public static int targetsNumber;

    //倒计时
    public Action PreRedayAction;
    public Action OnGameOver;

    [SerializeField] List<GameObject> allTarget;


    public static GameManager GetInstance()
    {
        if (_instance == null)
        {
            _instance = new GameManager();
        }
        return _instance;
    }

    private void Awake()
    {
        gameState = GameState.Over;

        globleUI = Instantiate(globleUI);

        alltargets = GameObject.FindObjectsOfType<Target>();

        targetsNumber = allTarget.Count;

        gameTime = gameTimeRest;

        _instance = this;

        ScoreProcess = Score;

        Application.targetFrameRate = frameRate;

        PreRedayAction += OnGameStart;

        OnGameOver += GameOver;

    }
    private void Update()
    {
        if (gameState == GameState.Start)
        {
            if (costTime>=amoutTime)
            {
                //OnGameOver?.Invoke();
                ShowStageFaildInfo();
            }
            else
            {
                costTime += Time.deltaTime;
            }
        }
    }

    void OnGameStart()
    {
        gameState = GameState.PreRedy;

        //玩家准备 开始倒计时
        StartCoroutine(OnPreStart());
    }

    //Coroutine showUICor = null;

    public IEnumerator OnPreStart()
    {
        PreStartUi.Instance.PearBillBord(true);
        OnPreReday();

        bool isReady = false;

        Action callback = () => {isReady = true;};

        //点头回调
        if (HeadMotionCapture.GetInstance() != null)
        {
            HeadMotionCapture.GetInstance().reayCallback = callback;
        }

        //检查点头
        while (true)
        {
            if (isReady)
            {
                break;
            }
            if (isDQ)
            {
                break;
            }
            //不点头，每隔三秒提示语音
            PreStartUi.Instance.BillbordInfo(readyWords, 0);
            yield return new WaitForSeconds(3f);
        }

        if (isDQ)
        {
            ShowStageFaildInfo();
        }
        else if (isReady)
        {
            //开始音效 stand by！
            yield return new WaitForSeconds(0.5f);

            PreStartUi.Instance.BillbordInfo("STAND BY!", 1);

            yield return new WaitForSeconds(1.5f);

            PreStartUi.Instance.BillbordInfo("", 2);

            //重置敌人
            //EnemyManager.EnemyReset?.Invoke();

            yield return new WaitForSeconds(0.5f);

            gameState = GameState.Start;

            //游戏时刻 反激活DQArea
            DQArea.DQAreaEnable(false);
            isDQ = false;

            PreStartUi.Instance.BillbordInfo("", 0);

            PreStartUi.Instance.PearBillBord(false);

            StartButton.EnableCollider(false);
        }
    }

    private void Score(int score)
    {
        playerScore += score;
    }

    [SerializeField] Target[] alltargets;


    [InspectFunction]
    public void GameReset()
    {
        gameState = GameState.Over;

        targetsNumber = allTarget.Count;

        //非游戏时刻提示收枪
        PreStartUi.Instance.PearBillBord(true);
        //PreStartUi.Instance.BillbordInfo("STAND START POINT BEGEN TRANING", 3);

        //重启所有Target
        TargetReset();

        //重置敌人
        EnemyManager.EnmeyDeadEvent?.Invoke();
        
        //重新激活开始位置
        StartButton.EnableCollider(true);

        //重值头部动作
        HeadMotionCapture.GetInstance().isReady = false;

        //清空统计击中的靶
        targetPassCount = 0;

        //清空统计的枪数
        fireCount = 0;

        //重置花费时间
        costTime = 0f;

        //非游戏时刻 激活DQArea
        DQArea.DQAreaEnable(true);

    }

    private void OnPreReday()
    {
        GameReset();
        FindObjectOfType<GunManeger>().ResetSocreText();
        playerScore = 0;
        GunManeger.amoutTime = 0f;
    }

    [InspectFunction]
    private void GameOver()
    {
        gameState = GameState.Over;

        //激活准备开始的游戏事件
        PreRedayAction = OnGameStart;

        isDQ = false;
        //开启
        StartButton.EnableCollider(true);
    }

    private void ShowStageFnishdInfo()
    {
        //激活PreStartUi
        PreStartUi.Instance.PearBillBord(true);

        //游戏结束UI这里要区分 stageFinished 跟faild两种情况的音效
        PreStartUi.Instance.VectorySound?.Invoke();

        //这里要区分 stageFinished 跟faild两种情况的文字
        PreStartUi.Instance.BillbordInfo(
            "STAGE FINISHED!\n\nSTAGE TIME: " + costTime.ToString("#0.00") + "s\n" 
            + " ROUNDS FIRED: " + fireCount+" (STANDARD ROUNDS: 17)"+"\n"
            +"NO SHOOT PENALTY: +"+ overloadTime.ToString("#0.00")+"s\n"
            +"TOTAL TIME: "+ (costTime+overloadTime).ToString("#0.00")+"s", 4);
    }



    public void ShowStageFaildInfo()
    {
        //激活PreStartUi
        PreStartUi.Instance.PearBillBord(true);

        //这里要区分 stageFinished 跟faild两种情况的文字
        PreStartUi.Instance.BillbordInfo("STAGE FAILED", 4);

        StartCoroutine(DelyGameOver());
    }

    IEnumerator DelyGameOver()
    {
        yield return new WaitForSeconds(1f);
        GameOver();
        PreStartUi.Instance.BillbordInfo("STAND START POINT BEGEN TRANING", 3);
    }


    void TargetReset()
    {
        if (alltargets.Length>=0)
        {
            for (int i = 0; i < alltargets.Length; i++)
            {
                alltargets[i].TargetResetAction?.Invoke();
                DynamicTargetLinkWith.ResetLinkTarget?.Invoke();
            }
        }
        //清空decal
        GunManeger.ClearBulletHole?.Invoke();
    }


   public void AddTargetPassCount()
    {
        if (targetPassCount> alltargets.Length-2)
        {
            ShowStageFnishdInfo();

            GameOver();
        }
        else
        {
            targetPassCount += 1;
        }
    }
}
