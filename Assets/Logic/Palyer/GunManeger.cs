﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PolyEngine;
using System;
using Ximmerse.RhinoX;

public class GunManeger : MonoBehaviour
{
    [SerializeField] GameObject bulltHull;
    [SerializeField] List<GameObject> bulletHullList;

    LineRenderer gunLaser;

    AudioSource audioSource;

    Animator anim;
    [SerializeField]
    GameObject scoreUI;

    [SerializeField]
    AudioClip gunFireSound;

    [SerializeField]
    Transform firePosition;

    [SerializeField]
    GameObject bulletShell;

    [SerializeField]
    GameObject gunLight;

    [SerializeField]
    GameObject hitFX;

    [SerializeField]
    GameObject smokeFX;

    [SerializeField]
    Transform bulletShellPosition;

    [SerializeField]
    Text text;

    [SerializeField]
    Text timeText;

    private int score =0 ;
    
    private void Awake()
    {
        //gunLaser = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();

        anim = GetComponent<Animator>();

        hitFX = Instantiate(hitFX);
        hitFX.SetActive(false);

        smokeFX = Instantiate(smokeFX);
        smokeFX.SetActive(false);

        bulletShell = GameObject.Instantiate(bulletShell);
        bulletShell.SetActive(false);

        scoreUI = Instantiate(scoreUI);
        scoreUI.GetComponent<Rigidbody>().isKinematic = true;
        scoreUI.SetActive(false);

        ClearBulletHole += ClearHole;
    }

    // Start is called before the first frame update
    void Start()
    {
        text.text = score.ToString();
    }

    RaycastHit hit;
    Ray ray = new Ray();

    public bool isReady = false;
    public static float amoutTime = 0f;

    // Update is called once per frame
    void Update()
    {
        if (InputTest.GetInstance().testMode == InputMode.Editor && InputTest.GetInstance().isTriggerTap)
        {
            Fire();
        }

        if (RXInput.IsButtonTap(RhinoXButton.ControllerTrigger))
        {
            Fire();
        }
    }
   
    void VisuralOnFire()
    {
        //StartCoroutine(LightGun());

        audioSource.PlayOneShot(gunFireSound);

        anim.SetTrigger("fire");

        PlayBulletShellAnimation();

        smokeFX.SetActive(true);
        smokeFX.transform.position = firePosition.position;
        smokeFX.GetComponent<ParticleSystem>().Play();
    }

    void SetBulletHull(Vector3 position,Vector3 normal,Transform parent)
    {
        var hull = Instantiate(bulltHull);
        hull.transform.position = position;
        hull.transform.forward = normal;
        hull.transform.position += hull.transform.forward * 0.001f;
        hull.transform.parent = parent;

        bulletHullList.Add(hull);
    }

    public static Action ClearBulletHole;

    void ClearHole()
    {
        if (bulletHullList.Count>0)
        {
            for (int i = 0; i < bulletHullList.Count; i++)
            {
                Destroy(bulletHullList[i].gameObject);
            }
            bulletHullList.Clear();
        }
    }

    void ShowScoreUIInHitPosition(Vector3 hitPoint,int score)
    {
        var rig = scoreUI.GetComponent<Rigidbody>();
        var text = scoreUI.GetComponentInChildren<Text>();
        //scoreUI.GetComponent<Rigidbody>().isKinematic = false;
        rig.isKinematic = true;

        scoreUI.transform.position = hitPoint;

        rig.isKinematic = true;

        scoreUI.transform.LookAt(this.gameObject.transform);

        if (score<0)
        {
            text.text = score.ToString();
            text.color = new Color(1.0f, 0.2f, 0f);
        }
        else
        {
            text.text ="+"+ score.ToString();
            text.color = new Color(1.0f, 1.0f, 1f);
        }

        scoreUI.SetActive(true);

        rig.isKinematic = false;

        rig.AddForce((Vector3.right+Vector3.up).normalized * 200f);

        StartCoroutine(DisableScoreUI());


    }


    IEnumerator ResetDQState(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GameManager.GetInstance().ShowStageFaildInfo();

        GameManager.GetInstance().isDQ = false;
    }

    public void Fire()
    {
        //检查是否存在DQ动作
        Action restGameByDQCallBack = () => 
        {

            Debug.Log($"<color=red>restGameByDQCallBack dq</color>");
            GameManager.GetInstance().isDQ = true;

            PreStartUi.Instance.BillbordInfo("   DQ   ", 3);

            StartCoroutine(ResetDQState(1f));
        };

        //如果含有DQ动作，挂载回调
        FindObjectOfType<DQArea>().checkPlayerDQMotion = restGameByDQCallBack;

        if (GameManager.GetInstance().gameState==GameState.Start)
        {
            //游戏开始后记录开枪数量
            GameManager.GetInstance().fireCount += 1;
        }

        VisuralOnFire();

        ray.origin = firePosition.position;

        ray.direction = firePosition.forward;

        //if (Physics.Raycast(ray, out hit))
        if (Physics.SphereCast(ray,0.07f,out hit,1000f))
        {

            var target = hit.collider.transform.GetComponent<Target>();
            var noshootTarget = hit.collider.transform.GetComponent<NoShootTarget>();

            //var linkTargrt = hit.collider.transform.GetComponent<DynamicTargetLinkWith>();

            if (target != null)
            {
                //PalyHitFX(hit.point);

                target.Hit?.Invoke();

                //Decal Set
                SetBulletHull(hit.point, hit.normal,hit.transform);

            }

            if (noshootTarget != null) 
            {
                noshootTarget.Hit?.Invoke();

                SetBulletHull(hit.point, hit.normal, hit.transform);
            }

        }
    }

    public void ResetSocreText()
    {
        text.text = "0";
    }

    [InspectFunction]
    void PlayBulletShellAnimation()
    {
        bulletShell.GetComponent<Rigidbody>().isKinematic = true;
        bulletShell.SetActive(true);

        bulletShell.transform.position = bulletShellPosition.position;
        bulletShell.transform.rotation = bulletShellPosition.rotation;

        bulletShell.GetComponent<Rigidbody>().isKinematic = false;
        bulletShell.GetComponent<Rigidbody>().AddForce(bulletShellPosition.forward * 50f, ForceMode.Force);
    }

    void PalyHitFX(Vector3 position)
    {
        if (hitFX!=null)
        {
            hitFX.SetActive(true);
            hitFX.transform.position = position;
            hitFX.GetComponent<ParticleSystem>().Play();

            StartCoroutine(WaitAlittleBit());
        }
    }

    IEnumerator WaitAlittleBit()
    {
        yield return new WaitForSeconds(0.5f);
        hitFX.SetActive(false);
    }

    IEnumerator DisableScoreUI()
    {
        yield return new WaitForSeconds(2f);
        scoreUI.SetActive(false);
    }

    IEnumerator LightGun()
    {
        gunLight.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        gunLight.SetActive(false);
    }
}
