﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellMovment : MonoBehaviour
{
    [SerializeField] GameObject cameraRig;
    Vector3 reletivePos;
    // Start is called before the first frame update
    void Start()
    {
        reletivePos = transform.position - cameraRig.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = cameraRig.transform.position + reletivePos;
    }
}
