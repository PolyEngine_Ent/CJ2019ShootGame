﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMotionCapture : MonoBehaviour
{
    private static HeadMotionCapture _instance;

    public static HeadMotionCapture GetInstance()
    {
        if (_instance==null)
        {
            return new HeadMotionCapture();
        }

        return _instance;
    }

    [SerializeField] GameObject head;

    Vector3 currentAngle;
    Vector3 lastAngle;

    public bool isReady = false;
    //public bool isShooterReady { get { return isReady; } }

    [Range(0f, 5f)]
    public float thershold;

    //public Action ShooterReay;

    StartButton startArea;
    private void Awake()
    {
        _instance = this;
        startArea = FindObjectOfType<StartButton>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    float degree =0f;

    public Action reayCallback = null;

    // Update is called once per frame
    void Update()
    {
        if (startArea != null && startArea.isPlayerOnStartArea)  
        {
            if (!isReady)
            {
                currentAngle = head.transform.localEulerAngles;
                degree = Mathf.Abs(currentAngle.x) - Mathf.Abs(lastAngle.x);

                Debug.Log(degree);

                if (degree >= thershold && degree > 0 && degree < 90f)
                {
                    reayCallback?.Invoke();
                    isReady = true;
                }
            }
        }

        lastAngle = head.transform.localEulerAngles;
    }
}
