﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using PolyEngine;
using System;



public class EnemyManager : MonoBehaviour
{

    NavMeshAgent agent;

    Animator anim;


    public float coverStayTime = 5f;

    public List<Transform> wayPoints;

    private Action EnmeyHitEvent;

    public static Action EnmeyDeadEvent;

    public static Action EnemyReset;

    private GameObject mPalyer;
    private void Awake()
    {
        fireTimer = fireTimerRest;

        mPalyer = GameObject.FindGameObjectWithTag("Player");

        timerRestart = coverStayTime;

        agent = GetComponent<NavMeshAgent>();

        anim = GetComponent<Animator>();

        //注册事件
        EnmeyHitEvent += EnemyHit;
        EnmeyDeadEvent += EnemyDeath;
        EnemyReset += ResetEnemyState;

    }

    private void OnDisable()
    {
        EnmeyHitEvent -= EnemyHit;
        EnmeyDeadEvent -= EnemyDeath;
    }

    int waypointIndex = 0;

    float timerRestart;
    bool itsTimeToGo = false;

    float fireTimer = 3f;
    public float fireTimerRest = 2f;

    bool isNeedFire = false;

    Quaternion targetRotation;

    bool isDead = false;


    private void Update()
    {
        if (agent.remainingDistance<agent.stoppingDistance && !isDead)
        {
            if (!anim.GetBool("walk"))
            {
                fireTimer -= Time.deltaTime;
                if (fireTimer < 0f)
                {
                    fireTimer = fireTimerRest;
                    anim.SetTrigger("shot");
                }
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, wayPoints[waypointIndex].rotation, Time.deltaTime * 5f);

            if (!itsTimeToGo)
            {
                timerRestart -= Time.deltaTime;
                if (timerRestart <= 0f)
                {
                    itsTimeToGo = true;
                    agent.SetDestination(wayPoints[LoopWayIndex()].position);
                }
            }
            else
            {
                timerRestart = coverStayTime;
                itsTimeToGo = false;
            }
        }
        else if (!isDead)
        {
            targetRotation = Quaternion.LookRotation(mPalyer.transform.position - transform.position);
            targetRotation.PitchByAngle(0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 10f);
        }

    }

    private void LateUpdate()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Shooting Pistol"))
        {
            agent.isStopped = true;
        }
        else
        {
            agent.isStopped = false;
        }
        anim.SetFloat("vx", agent.velocity.x);
        anim.SetFloat("vz", agent.velocity.z);
        //anim.SetFloat("rx", agent.angularSpeed);

        if (agent.velocity==Vector3.zero)
        {
            anim.SetBool("walk", false);
        }
        else
        {
            anim.SetBool("walk", true);
        }
    }

    [InspectFunction]
    IEnumerator Reset(float time)
    {
        yield return new WaitForSeconds(time);

        agent.isStopped = false;
        agent.updatePosition = true;
        agent.updateRotation = true;

        isDead = false;

        anim.Play("locomotion");
        anim.Update(0);

        transform.position = wayPoints[0].position;

        waypointIndex = 0;

        timerRestart = coverStayTime;

        fireTimer = fireTimerRest;

    }
    private void ResetEnemyState()
    {
        StartCoroutine(Reset(1f));
    }

    bool needInverse = false;
    private int LoopWayIndex()
    {
        if (waypointIndex >= 0 && !needInverse)
        {
            if (waypointIndex >= wayPoints.Count-1)
            {
                needInverse = true;
            }
            else
            {
                waypointIndex += 1;
            }
        }
        if (needInverse)
        {
            if (waypointIndex <= 0)
            {
                needInverse = false;
            }
            else
            {
                waypointIndex -= 1;
            }

        }
        return waypointIndex;
    }

    private void EnmeyShot()
    {
        //agent.isStopped = true;
        //anim.SetTrigger("shot");
        //StartCoroutine(ResetAgent(0.5f));
    }

    //IEnumerator ResetAgent(float delayTime)
    //{
    //    yield return new WaitForSeconds(delayTime);
    //    agent.isStopped = false;
    //}

    [InspectFunction]
    public void EnemyHit()
    {

    }

    [InspectFunction]
    public void EnemyDeath()
    {
        isDead = true;

        agent.isStopped = true;
        agent.updatePosition = false;
        agent.updateRotation = false;

        //StartCoroutine(Reset(2f));
    }
}
