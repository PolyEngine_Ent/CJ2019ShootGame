﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine;
using System;

public class StartButton : MonoBehaviour
{
    public bool isPlayerOnStartArea = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player" && GameManager.GetInstance().gameState == GameState.Over)
        {
            if (GameManager.GetInstance().isDQ)
            {
                GameManager.GetInstance().isDQ = false;
                GameManager.GetInstance().ShowStageFaildInfo();
            }
            else
            {

                GameManager.GetInstance().GameReset();

                isPlayerOnStartArea = true;

                //确保只执行一次，然后卸载掉事件，避免每次进入调用
                GameManager.GetInstance().PreRedayAction?.Invoke();
                GameManager.GetInstance().PreRedayAction = null;
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.GetInstance().GameReset();
            isPlayerOnStartArea = false;
        }
    }

    [InspectFunction]
    public static void EnableCollider(bool isActive)
    {
        if (!isActive)
        {
            FindObjectOfType<StartButton>().isPlayerOnStartArea = false;
        }

        FindObjectOfType<StartButton>().gameObject.GetComponent<SphereCollider>().enabled = isActive;
    }
}
