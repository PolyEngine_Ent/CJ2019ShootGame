﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoBackSelectedPage : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            GameManager.GetInstance().GameReset();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}
