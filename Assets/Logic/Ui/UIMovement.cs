﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMovement : MonoBehaviour
{
    GameObject player;

    float distance;
    Vector3 dirction = new Vector3();
    Vector3 curVelocity = new Vector3();
    // Start is called before the first frame update
    void Awake()
    {
       
    }
    private void Start()
    {
        if (InputTest.GetInstance().testMode!=InputMode.RX)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            player = GameObject.Find("AR Camera").gameObject;
        }

        distance = Vector3.Distance(this.transform.position , player.transform.position);
    }   
    // Update is called once per frame
    void Update()
    {
        dirction = (player.transform.forward - transform.forward).normalized;
        this.transform.position = Vector3.SmoothDamp (transform.position, player.transform.position + dirction * distance,ref curVelocity, Time.deltaTime,10f);
        this.transform.LookAt(player.transform, Vector3.up);
    }
}
