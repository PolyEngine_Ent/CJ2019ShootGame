﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestatButton : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        DynamicTargetLinkWith.ResetLinkTarget?.Invoke();
        if (other.tag == "Player" && GameManager.GetInstance().gameState == GameState.Start)
        {
            GameManager.GetInstance().gameState = GameState.Over;
            GameManager.GetInstance().GameReset();
        }
    }
}
