﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PreStartUi : MonoBehaviour
{
    private static PreStartUi _instance;

    public static PreStartUi Instance
    {
        get
        {
            if (_instance==null)
            {
                return new PreStartUi();
            }
            else
            {
                return _instance;
            }
        }
    }

    [SerializeField] Text text;
    [SerializeField] AudioClip clip;
    [SerializeField] AudioClip preStartClip;
    [SerializeField] AudioClip prestartTipsAudio;
    [SerializeField] AudioClip vectoryAudio;

    AudioSource source;
    public Action VectorySound;
    public Action<string,int> BillbordInfo;
    public Action<bool> PearBillBord;

    private void Awake()
    {
        text.text = "STAND START POINT BEGEN TRANING";
        _instance = this;

        BillbordInfo += BillbordText;
        PearBillBord += PearBord;

        source = GetComponent<AudioSource>();
        VectorySound += PlayStartSound;
        //text.text = GameManager.gameInfoText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void BillbordText(string str , int palySound = 0)
    {
        switch (palySound)
        {
            case 0:
                source.PlayOneShot(prestartTipsAudio);
                break;
            case 1:
                source.PlayOneShot(preStartClip);
                break;
            case 2:
                source.PlayOneShot(clip);
                break;
            default:
                break;
        }

        text.text = str;
    }

    private void PlayStartSound()
    {
        source.PlayOneShot(vectoryAudio);
    }

    private void OnDisable()
    {
        BillbordInfo -= BillbordText;
    }

    private void OnEnable()
    {
        BillbordInfo += BillbordText;
        PearBillBord += PearBord;
    }

    private void PearBord(bool isPear)
    {
        this.transform.parent.gameObject.SetActive(isPear);
    }

}
