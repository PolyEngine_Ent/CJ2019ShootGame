﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine;
using UnityEngine.SceneManagement;

public class Stage : MonoBehaviour
{
    public string sceneName;

    [InspectFunction]
   public void EnterStage()
    {
        StartCoroutine(Delay());
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.2f);
        SceneManager.LoadScene(sceneName);
    }
}
