﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DynamicTargetLinkWith : MonoBehaviour
{
    [SerializeField] GameObject linkObject;

    Animator linkObjectAniamtor;
    Target target;

    public static Action ResetLinkTarget;
    public static Action DynamicTargrtHit;

    private void Awake()
    {
        target = this.GetComponent<Target>();
        //DynamicTargrtHit += Hit;
        ResetLinkTarget += Reset;

        linkObjectAniamtor = linkObject == null ? null : linkObject.GetComponent<Animator>();
    }

    private void Start()
    {
        target.linktargetHit += Hit;
    }

    void Reset()
    {
        if (linkObjectAniamtor!=null)
        {
            linkObjectAniamtor.SetTrigger("reset");
        }
    }
    void Hit()
    {
        linkObjectAniamtor.SetTrigger("hit");
    }
}
