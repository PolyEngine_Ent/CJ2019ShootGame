﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine;

public struct WithRigOrigonTransformInfo
{
    public Vector3 position;
    public Quaternion rotation;
}
public class Target : MonoBehaviour
{
    [SerializeField] int maxPassCount;

    [SerializeField] int passCount = 0;

    [SerializeField] Animator anim;

    [SerializeField] Rigidbody rig;

    public bool isPassed = false;

    public Action TargetResetAction;
    public Action Hit;

    WithRigOrigonTransformInfo origionInfo;

    [InspectFunction]
     void ResetTest()
    {
        TargetResetAction?.Invoke();
    }


    private void Awake()
    {
        if (anim != null)
        {
            TargetResetAction = ResetWithAnimator;

            Hit = HitTargetWithAnimator;
        }
        else if (rig != null)
        {
            origionInfo = new WithRigOrigonTransformInfo();

            origionInfo.position = transform.position;
            origionInfo.rotation = transform.rotation;

            TargetResetAction = ResetWithRig;

            Hit = HitTargetWithRigibody;
        }
        else
        {
            TargetResetAction = Reset;

            Hit = HitTarget;
        }

    }

    public Action linktargetHit = null;

    void HitTarget()
    {

            if (passCount < maxPassCount - 1)
            {
                passCount += 1;
            }
            else
            {
                passCount = maxPassCount;

                isPassed = true;

                this.gameObject.GetComponent<MeshCollider>().enabled = false;

                GameManager.GetInstance().AddTargetPassCount();

                linktargetHit?.Invoke();

            }


    }
    void HitTargetWithAnimator()
    {

            if (passCount < maxPassCount - 1)
            {
                passCount += 1;
            }
            else
            {
                passCount = maxPassCount;

                isPassed = true;

                this.gameObject.GetComponent<MeshCollider>().enabled = false;

                GameManager.GetInstance().AddTargetPassCount();

                anim.SetTrigger("hit");

                linktargetHit?.Invoke();
            }

    }
    void HitTargetWithRigibody()
    {
            if (passCount < maxPassCount - 1)
            {
                passCount += 1;
            }
            else
            {
                passCount = maxPassCount;

                isPassed = true;

                this.gameObject.GetComponent<MeshCollider>().enabled = false;

                GameManager.GetInstance().AddTargetPassCount();

                linktargetHit?.Invoke();

                rig.isKinematic = false;

                var randomPos = new Vector3(transform.position.x + UnityEngine.Random.Range(-0.1f, 0.1f), transform.position.y + UnityEngine.Random.Range(-0.1f, 0.1f), transform.position.z + UnityEngine.Random.Range(-0.1f, 0.1f));
                rig.AddExplosionForce(UnityEngine.Random.Range(100f,300f), randomPos, 1f, 0f, ForceMode.Force);
            }
    }

    private void Reset()
    {
        if (this.gameObject.GetComponent<MeshCollider>()!=null)
        {
            this.gameObject.GetComponent<MeshCollider>().enabled = true;
        }

        isPassed = false;
        passCount = 0;
    }
    private void ResetWithAnimator()
    {
        if (this.gameObject.GetComponent<MeshCollider>() != null)
        {
            this.gameObject.GetComponent<MeshCollider>().enabled = true;
        }

        isPassed = false;
        passCount = 0;

        anim.SetTrigger("reset");
    }
    private void ResetWithRig()
    {
        if (this.gameObject.GetComponent<MeshCollider>() != null)
        {
            this.gameObject.GetComponent<MeshCollider>().enabled = true;
        }

        isPassed = false;
        passCount = 0;

        rig.isKinematic = true;

        transform.position = origionInfo.position;
        transform.rotation = origionInfo.rotation;
    }
}
