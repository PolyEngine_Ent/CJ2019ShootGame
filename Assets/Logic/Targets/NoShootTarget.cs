﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoShootTarget : MonoBehaviour
{
    public float payTimer = 10f;
    public Action Hit;
    // Start is called before the first frame update
    void Awake()
    {
        Hit = HitNoshoot;
    }

    void HitNoshoot()
    {
        GameManager.GetInstance().overloadTime += payTimer;
    }
}
