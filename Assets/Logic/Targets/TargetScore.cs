﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScore : MonoBehaviour
{
    [SerializeField] int targetSocre = 5;
    [SerializeField] int hitSoundIndex = 0;

    [SerializeField] AudioClip audioClips;

    [SerializeField] AudioSource audioSource;

    public static Action ResetTargetAction = null;

    Animator anim;
    
    private void Awake()
    {
        //anim = TopParent().GetComponent<Animator>();
        ResetTargetAction += ResetAnimator;
    }

    bool selfHit = false;
    private void Update()
    {
        // selfHit = anim.GetCurrentAnimatorStateInfo(0).IsName("hit");

        //if (selfHit)
        //{
        //    mCurrentLevel.InBlockTargetHitInfo();
        //    selfHit = false;
        //}
    }

    void ResetAnimator()
    {       
        if(TopParent().GetComponent<Animator>()!=null)
        {
            TopParent().GetComponent<Animator>().SetTrigger("reset");
        }
    }

    public int GetTargetScore()
    {
        return targetSocre;
    }

    public GameObject TopParent()
    {
        GameObject go = null;

        GameObject tmp = this.gameObject;

        while (true)
        {
            if (tmp.transform.parent != null) 
            {
                tmp = tmp.transform.parent.gameObject;
                go = tmp;
            }
            else
            {
                break;
            }
        }
        return go;
    }
    public void PlayShotSound()
    {
        if (audioClips!=null)
        {
            audioSource.PlayOneShot(audioClips);
        }

    }
}
