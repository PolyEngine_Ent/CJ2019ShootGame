﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;


namespace PolyEngine.ArShootTraining
{
    /// <summary>
    /// Static target.
    /// </summary>
    public class StaticTarget : MonoBehaviour , I_Target
    {
        static List<StaticTarget> AllStaticTargets = new List<StaticTarget>();

        public int Score = 10;

        public bool OnShootEnds = true;

        public bool AutoRespawn = true;

        public float RespawnTimeMin = 5;

        public float RespawnTimeMax = 10;

        public string DeathTrigger = "hit";

        public string ResetTrigger = "reset";

        bool m_IsAlive = true;
        [InspectFunction]
        public bool IsAlive
        {
            get
            {
                return m_IsAlive;
            }
        }

        /// <summary>
        /// The target animator.
        /// </summary>
        public Animator m_Animator;

        float m_DeathTime = 0;

        /// <summary>
        /// Gets the death time.
        /// </summary>
        /// <value>The death time.</value>
        public float DeathTime
        {
            get
            {
                return m_DeathTime;
            }
        }

        void Awake()
        {
            AllStaticTargets.Add(this);
        }

        void OnDestroy()
        {
            AllStaticTargets.Remove(this);
        }

        /// <summary>
        /// On hit method.
        /// </summary>
        /// <param name="hitInfo">Hit info.</param>
        [InspectFunction]
        public int OnHit(RaycastHit hitInfo)
        {
            Player.OnPlayerShootGainScore(this.Score);
            if(OnShootEnds)
            {
                m_DeathTime = Time.time;
                if(AutoRespawn)
                {
                    Invoke("Respawn", Random.Range(RespawnTimeMin, RespawnTimeMax));
                }
                m_IsAlive = false;
                PlayDeath();
            }
            return Score;
        }

        [InspectFunction]
        void Respawn ()
        {
            ReActive();
            PlayReset();
        }

        void PlayDeath ()
        {
            if(m_Animator)
            {
                m_Animator.SetTrigger(DeathTrigger);
            }
        }

        public void ReActive()
        {
            m_IsAlive = true;

        }

        void PlayReset ()
        {
            if (m_Animator)
            {
                m_Animator.SetTrigger(ResetTrigger);
            }
        }

#if UNITY_EDITOR
        [ContextMenu ("Find animator")]
        public void FindAnimator ()
        {
            if(m_Animator == null)
               m_Animator = GetComponentInParent<Animator>();

            if (m_Animator == null)
                m_Animator = GetComponentInChildren<Animator>();
        }
#endif
    }
}