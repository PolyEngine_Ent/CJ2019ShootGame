﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
namespace PolyEngine.ArShootTraining
{
    /// <summary>
    /// Game start trigger.
    /// Using RhinoX trackable identities to automatically starts the game.
    /// </summary>
    public class GameStartTrigger : MonoBehaviour
    {
        public float RequireTrackedTimeToStartGame = 0.5f;

        [SerializeField]
        TrackableIdentity[] identities = new TrackableIdentity[]
        {

        };

        float visibleTime = 0;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(CJ2019GameManager.Phase == GamePhase.NotStart)
            {
                if(ScansMarkers())
                {
                    CJ2019GameManager.Instance.StartGame();
                }
            }
        }

        bool ScansMarkers ()
        {
            bool isTracked = false;
            foreach(var marker in identities)
            {
                if(marker.IsVisible)
                {
                    visibleTime += Time.deltaTime;
                    isTracked = true;
                    break;
                }
            }

            if(isTracked && visibleTime >= RequireTrackedTimeToStartGame)
            {
                visibleTime = 0;
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}