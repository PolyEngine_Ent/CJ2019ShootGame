﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolyEngine.ArShootTraining
{
    public struct ShootEvent
    {
        public bool isHit;

        public GameObject hitObject;
    }
    /// <summary>
    /// Player script.
    /// </summary>
    public class Player : SingletonMonobehaviour<Player>
    {
        int m_Score = 0;
        [InspectFunction]
        public int Score
        {
            get
            {
                return m_Score;
            }
            set
            {
                m_Score = value;
                OnPlayerScoreChange?.Invoke(value);
            }
        }

        int m_ShootCount;

        [InspectFunction]
        public int ShootCount
        {
            get
            {
                return m_ShootCount;
            }
            set
            {
                m_ShootCount = value;
            }
        }

        int m_HitCount;

        [InspectFunction]
        public int HitCount
        {
            get
            {
                return m_HitCount;
            }
            set
            {
                m_HitCount = value;
            }
        }

        /// <summary>
        /// Gets the hit rate.
        /// 命中率， 0 - 100
        /// </summary>
        /// <value>The hit rate.</value>
        public int HitRate
        {
            get
            {
                if (m_ShootCount == 0)
                    return 0;
                return (int)(((float)m_HitCount / (float)m_ShootCount) * 100);
            }
        }

        public static event System.Action<int> OnPlayerScoreChange = null;


        public static void OnPlayerShoot (ShootEvent shootEvent)
        {
            Instance.m_ShootCount++;
            if(shootEvent.isHit)
            {
                Instance.m_HitCount++;
            }
        }

        public static void OnPlayerShootGainScore (int score)
        {
            Instance.Score += score;
        }

        protected override void Awake()
        {
            base.Awake();
            CJ2019GameManager.OnGamePhaseChange += CJ2019GameManager_OnGamePhaseChange;
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            CJ2019GameManager.OnGamePhaseChange -= CJ2019GameManager_OnGamePhaseChange;
        }

        void CJ2019GameManager_OnGamePhaseChange(GamePhase phase)
        {
            if(phase == GamePhase.NotStart)
            {
                m_Score = m_HitCount = m_ShootCount = 0;
            }
        }

        public void SerializePlayerData (byte[] buffer, int offset, out int count)
        {
            offset = PEUtils.WriteInt((int)CJ2019GameManager.Phase, buffer, offset);
            offset = PEUtils.WriteInt(Score, buffer, offset);
            offset = PEUtils.WriteInt(HitCount, buffer, offset);
            offset = PEUtils.WriteInt(HitRate, buffer, offset);
            count = 16;
        }
    }
}