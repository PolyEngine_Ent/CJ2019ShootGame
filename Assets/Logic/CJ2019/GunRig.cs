﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;


namespace PolyEngine.ArShootTraining
{
    /// <summary>
    /// Gun rig.
    /// </summary>
    public class GunRig : MonoBehaviour
    {
        [SerializeField]
        Animator m_GunModel;

        [SerializeField]
        AudioSource m_GunShootSFX;

        [SerializeField]
        GameObject m_BulletShellPrefab;

        [SerializeField]
        GameObject m_smokeFX;

        [SerializeField]
        Transform m_MuzzleAnchor;

        [SerializeField]
        Transform bulletShellSpawnAnchor;

        int bulletShellInstanceIndex = 0;
        GameObject[] m_BulletShellInstances = new GameObject[5];

        [SerializeField, Range(0, 0.1f)]
        float m_ShootCastRadius = 0.07f;

        [SerializeField]
        LayerMask m_TargetLayerMask = -1;

        [SerializeField] 
        GameObject m_Decal;

        [SerializeField]
        ScoreUI m_ScorePrefab;

        // Start is called before the first frame update
        void Awake()
        {
            for(int i=0; i< m_BulletShellInstances.Length; i++)
            {
                m_BulletShellInstances[i] = Instantiate<GameObject>(m_BulletShellPrefab);
                m_BulletShellInstances[i].SetActive(false);
            }
           
        }

        /// <summary>
        /// On player shoot action.
        /// </summary>
        public void OnPlayerShoot ()
        {
            if(CJ2019GameManager.Phase == GamePhase.Started || CJ2019GameManager.Phase == GamePhase.Ending_CountDown)
            {
                Fire();
            }
        }

        [ContextMenu("Fire")]
        public void Fire()
        {
            Raycasting();
            PlayShootFX();
        }

        [ContextMenu ("Play Shoot FX")]
        void PlayShootFX()
        {
            //StartCoroutine(LightGun());

            m_GunShootSFX.PlayOneShot(m_GunShootSFX.clip);

            m_GunModel.SetTrigger("fire");

            PlayBulletShellAnimation();

            var muzzleFX = Instantiate<GameObject>(m_smokeFX, m_MuzzleAnchor.position, m_MuzzleAnchor.rotation);
            muzzleFX.SetActive(true);
            muzzleFX.GetComponent<ParticleSystem>().Play();
        }

        [ContextMenu ("Play Bullet Shell Animation")]
        async Task PlayBulletShellAnimation()
        {
            var bulletShell = m_BulletShellInstances[bulletShellInstanceIndex];
            bulletShellInstanceIndex = (bulletShellInstanceIndex + 1) % m_BulletShellInstances.Length;
            bulletShell.GetComponent<Rigidbody>().isKinematic = true;
            bulletShell.SetActive(true);
            bulletShell.transform.SetPositionAndRotation(bulletShellSpawnAnchor.position, bulletShellSpawnAnchor.rotation);
            bulletShell.GetComponent<Rigidbody>().isKinematic = false;
            bulletShell.GetComponent<Rigidbody>().AddForce(bulletShellSpawnAnchor.forward * 50f, ForceMode.Force);
            await Task.Delay(1000);
            bulletShell.SetActive(false);
        }

        void Raycasting ()
        {
            Ray ray = new Ray(m_MuzzleAnchor.position, m_MuzzleAnchor.forward);
            RaycastHit hit = default(RaycastHit);
            bool isHit = false;
            if (m_ShootCastRadius > 0)
            {
                isHit = Physics.SphereCast(ray, 0.07f, out hit, 1000f, m_TargetLayerMask, QueryTriggerInteraction.UseGlobal);
            }
            else
            {
                isHit = Physics.Raycast(ray, out hit, 1000f, m_TargetLayerMask, QueryTriggerInteraction.UseGlobal);
            }
            bool doHit = false;
            GameObject doHitGO = null;
            if (isHit)
            {
                //var target = hit.collider.transform.GetComponent<Target>();
                //var noshootTarget = hit.collider.transform.GetComponent<NoShootTarget>();

                //var linkTargrt = hit.collider.transform.GetComponent<DynamicTargetLinkWith>();
                var target = hit.transform.GetComponent<I_Target>();

                if (target != null && target.IsAlive)
                {
                    doHit = true;
                    doHitGO = hit.transform.gameObject;
                    int _score = target.OnHit(hit);

                    if(m_ScorePrefab)
                    {
                        var scoreComponent = Instantiate<GameObject>(m_ScorePrefab.gameObject, hit.point, m_ScorePrefab.transform.rotation);
                        scoreComponent.GetComponent<ScoreUI>().text.text = _score.ToString();
                    }
                    //PalyHitFX(hit.point);

                    //target.Hit?.Invoke();

                    //Decal Set
                    //SetBulletHull(hit.point, hit.normal, hit.transform);


                }
                CreateDecal(hit.point, hit.normal, hit.transform);
                //if (noshootTarget != null)
                //{
                //    noshootTarget.Hit?.Invoke();

                //    //SetBulletHull(hit.point, hit.normal, hit.transform);
                //    CreateDecal(hit.point, hit.normal, hit.transform);
                //}

            }
            ShootEvent shootEvent = new ShootEvent()
            {
                isHit = doHit,
                hitObject = doHitGO,
            };
            Player.OnPlayerShoot(shootEvent);
        }

        void CreateDecal(Vector3 position, Vector3 normal, Transform parent)
        {
            var decalInstance = Instantiate(m_Decal);
            decalInstance.transform.position = position;
            decalInstance.transform.forward = normal;
            decalInstance.transform.position += decalInstance.transform.forward * 0.001f;
            decalInstance.transform.parent = parent;
            Destroy(decalInstance, 5);
        }
    }
}