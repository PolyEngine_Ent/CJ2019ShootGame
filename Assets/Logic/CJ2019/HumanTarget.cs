﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace PolyEngine.ArShootTraining
{
    public class HumanTarget : MonoBehaviour, I_Target
    {

        public int HP = 90;

        [SerializeField]
        Animator m_Animator;

        [SerializeField]
        bool m_IsAlive = false;

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:PolyEngine.ArShootTraining.HumanTarget"/> is alive.
        /// </summary>
        /// <value><c>true</c> if is alive; otherwise, <c>false</c>.</value>
        public bool IsAlive
        {
            get
            {
                return this.m_IsAlive;
            }
        }

        public int OnHit(RaycastHit hitInfo)
        {
            return this.HP;
        }

        public void ReActive()
        {

        }

        public void OnHitBodyPart (BodyPart bodyPart)
        {
            if (!m_IsAlive)
                return;

            HP -= bodyPart.Health;
            Player.OnPlayerShootGainScore(bodyPart.Health);
            if (HP <= 0)
            {
                m_Animator.SetTrigger("death");
                GetComponent<NavMeshAgent>().enabled = false;
                m_IsAlive = false;
            }
            else
            {
                m_Animator.SetTrigger("hit");
            }
        }
    }
}