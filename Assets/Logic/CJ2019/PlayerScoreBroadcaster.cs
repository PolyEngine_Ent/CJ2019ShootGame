﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine.UnityNetworking;

namespace PolyEngine.ArShootTraining
{
    public class PlayerScoreBroadcaster : MonoBehaviour
    {
        public UDPServer m_udpServer;

        public LANBroadcaster m_LANBroadcaster;

        public float BroadcasterInterval = 1.5f;

        float previousBroadcastTime = 0;

        byte[] buffer = new byte[16];

        // Start is called before the first frame update
        void Awake()
        {
            m_LANBroadcaster.StartMode = LANBroadcaster.BroadcastMode.Script;
            m_udpServer.AutoStart = true;
        }

        // Update is called once per frame
        void Start()
        {
            m_LANBroadcaster.BroadcastText = m_udpServer.ServerPort.ToString();
            m_LANBroadcaster.StartBroadcast();
        }


        private void Update()
        {
            if((Time.time - previousBroadcastTime) >= BroadcasterInterval)
            {
                Player.Instance.SerializePlayerData(buffer, 0, out int count);
                m_udpServer.Broadcast(buffer, count, 0);
                previousBroadcastTime = Time.time;
            }

        }
    }
}