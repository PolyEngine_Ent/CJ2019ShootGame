﻿using UnityEngine;

namespace PolyEngine.ArShootTraining
{
    /// <summary>
    /// Interface : target.
    /// </summary>
    public interface I_Target
    {
        /// <summary>
        /// On hit method.
        /// </summary>
        /// <param name="hitInfo">Hit info.</param>
        int OnHit(RaycastHit hitInfo);

        /// <summary>
        /// reactive
        /// </summary>
        void ReActive();

        /// <summary>
        /// Is the target alive ?
        /// </summary>
        /// <value><c>true</c> if is alive; otherwise, <c>false</c>.</value>
        bool IsAlive { get; }
    }
}