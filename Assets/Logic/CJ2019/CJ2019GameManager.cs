﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;

namespace PolyEngine.ArShootTraining
{
    public enum GamePhase
    {
        /// <summary>
        /// 游戏还未开始
        /// </summary>
        NotStart,

        /// <summary>
        /// 看见 marker 之后进入 prestart 状态
        /// </summary>
        PreStart,

        /// <summary>
        /// 开始倒计时状态
        /// </summary>
        Starting_CountDown,

        /// <summary>
        /// 已经开始
        /// </summary>
        Started,

        /// <summary>
        /// 结束倒计时
        /// </summary>
        Ending_CountDown,

        /// <summary>
        /// 已经结束
        /// </summary>
        Ended,
    }
    /// <summary>
    /// CJ 2019 Game manager.
    /// </summary>
    public class CJ2019GameManager : SingletonMonobehaviour<CJ2019GameManager>
    {
        [SerializeField]
        GamePhase m_Phase = GamePhase.NotStart;

        /// <summary>
        /// Gets the phase.
        /// </summary>
        /// <value>The phase.</value>
        public static GamePhase Phase
        {
            get
            {
                return Instance.m_Phase;
            }
        }

        [SerializeField, Min(10)]
        float m_GameRoundTime = 60;

        public GameObject StaticScene;

        public AudioSource startCountDownSFX, startVoiceSFX, endCountDownSFX, endGameSFX;

        public static event System.Action<int> PreStartCountDownEvent = null;

        public static event System.Action GameStartedEvent = null;

        public static event System.Action GameEndedEvent = null;

        public static event System.Action<GamePhase> OnGamePhaseChange = null;


        public static event System.Action<int> PreEndCountDownEvent = null;


        float startGameTime = 0;

        // Start is called before the first frame update
        void Start()
        {
            ChangeOnPhase();
        }

        [InspectFunction]
        public async void StartGame()
        {
            SetPhase(GamePhase.PreStart);
            await Task.Delay(1000);
            SetPhase(GamePhase.Starting_CountDown);
            for(int i=5; i>=1; i--)
            {
                PreStartCountDownEvent?.Invoke(i);
                startCountDownSFX.Play();
                await Task.Delay(1000);
            }
            await Task.Delay(1000);
            SetPhase(GamePhase.Started);
            startGameTime = Time.time;
            startVoiceSFX.Play();
            GameStartedEvent?.Invoke();
        }

        [InspectFunction]
        public async void EndGame()
        {
            SetPhase(GamePhase.Ending_CountDown);
            await Task.Delay(1000);
            for (int i = 5; i >= 1; i--)
            {
                PreEndCountDownEvent?.Invoke(i);
                endCountDownSFX.Play();
                await Task.Delay(1000);
            }
            await Task.Delay(1000);
            SetPhase(GamePhase.Ended);
            endGameSFX.Play();
            GameEndedEvent?.Invoke();
        }

        [InspectFunction]
        public void SetPhase (GamePhase phase)
        {
            if(m_Phase != phase)
            {
                m_Phase = phase;
                ChangeOnPhase();
            }
        }



        void ChangeOnPhase ()
        {
            switch (m_Phase)
            {
                case GamePhase.NotStart:
                    StaticScene.SetActive(false);
                    break;

                case GamePhase.PreStart:
                    StaticScene.SetActive(true);
                    break;

                case GamePhase.Starting_CountDown:
                    break;

                case GamePhase.Ended:
                    break;

                case GamePhase.Started:
                    break;

                case GamePhase.Ending_CountDown:
                    break;
            }
            OnGamePhaseChange?.Invoke(m_Phase);
        }

        private void Update()
        {
             if(m_Phase == GamePhase.Started)
            {
                if((Time.time - startGameTime) >= m_GameRoundTime)
                {
                    EndGame();
                }
            }
        }
    }
}