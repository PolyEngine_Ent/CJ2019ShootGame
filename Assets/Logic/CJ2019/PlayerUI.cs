﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading.Tasks;
namespace PolyEngine.ArShootTraining
{
    public class PlayerUI : MonoBehaviour
    {
        public TextMeshProUGUI m_ScoreText, m_StartCountDownText, m_EndCountDownText, m_SummaryText;


        private void Awake()
        {
            InitComponents();
            CJ2019GameManager.OnGamePhaseChange += CJ2019GameManager_OnGamePhaseChange;
            CJ2019GameManager.PreStartCountDownEvent += CJ2019GameManager_CountDownEvent;
            CJ2019GameManager.PreEndCountDownEvent += CJ2019GameManager_PreEndCountDownEvent;
            Player.OnPlayerScoreChange += Player_OnPlayerScoreChange;
        }

        async void CJ2019GameManager_PreEndCountDownEvent(int countDown)
        {
            m_EndCountDownText.text = countDown.ToString();
            m_EndCountDownText.gameObject.SetActive(true);
            await Task.Delay(1000);
            m_EndCountDownText.gameObject.SetActive(false);
        }


        void InitComponents()
        {
            m_ScoreText.gameObject.SetActive(false);
            m_StartCountDownText.gameObject.SetActive(false);
            m_EndCountDownText.gameObject.SetActive(false);
            m_SummaryText.gameObject.SetActive(false);
        }

        async void CJ2019GameManager_CountDownEvent(int countDown)
        {
            m_StartCountDownText.text = countDown.ToString();
            m_StartCountDownText.gameObject.SetActive(true);
            await Task.Delay(1000);
            m_StartCountDownText.gameObject.SetActive(false);
        }


        void CJ2019GameManager_OnGamePhaseChange(GamePhase phase)
        {
            if(phase == GamePhase.NotStart)
            {
                InitComponents();
            }
            else if (phase == GamePhase.Started)
            {
                m_ScoreText.gameObject.SetActive(true);
            }
            else if(phase == GamePhase.Ended)
            {
                m_ScoreText.gameObject.SetActive(false);
                m_StartCountDownText.gameObject.SetActive(false); 
                m_EndCountDownText.gameObject.SetActive(false);
                m_SummaryText.gameObject.SetActive(true);
                m_SummaryText.text = string.Format("您的得分: {0}\n命中率: {1}%", Player.Instance.Score, Player.Instance.HitRate);
            }
        }


        void Start()
        {
        }

        private void OnDestroy()
        {
            Player.OnPlayerScoreChange -= Player_OnPlayerScoreChange;
            CJ2019GameManager.OnGamePhaseChange -= CJ2019GameManager_OnGamePhaseChange;
            CJ2019GameManager.PreStartCountDownEvent -= CJ2019GameManager_CountDownEvent;
        }

        void Player_OnPlayerScoreChange(int score)
        {
            m_ScoreText.text = score.ToString();
        }
    }
}