﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PolyEngine.ArShootTraining
{
    public class BodyPart : MonoBehaviour, I_Target
    {
        public HumanTarget human;

        public int Health = 30;

        /// <summary>
        /// Gets a value indicating whether this <see cref="T:PolyEngine.ArShootTraining.BodyPart"/> is alive.
        /// </summary>
        /// <value><c>true</c> if is alive; otherwise, <c>false</c>.</value>
        public bool IsAlive
        {
            get
            {
                return human.IsAlive;
            }
        }

        [ContextMenu ("On hit")]
        public int OnHit(RaycastHit hitInfo)
        {
            human.OnHitBodyPart(this);
            return Health;
        }

        public void ReActive()
        {

        }

#if UNITY_EDITOR
        [ContextMenu ("Find human target")]
        void FindHumanTarget()
        {
            if(human==null)
            human = GetComponentInParent<HumanTarget>();
        }
#endif
    }
}