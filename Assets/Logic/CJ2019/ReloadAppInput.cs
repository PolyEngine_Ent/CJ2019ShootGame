﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
namespace PolyEngine.ArShootTraining
{
    public class ReloadAppInput : MonoBehaviour
    {
        public void ReloadApp()
        {
            if(CJ2019GameManager.Phase == GamePhase.Ended)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }
        }
    }
}