﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DQArea : MonoBehaviour
{
    public static void DQAreaEnable(bool enabled)
    {
        FindObjectOfType<DQArea>().GetComponent<BoxCollider>().enabled = enabled;
    }

    public Action checkPlayerDQMotion = null;

    private void OnTriggerEnter(Collider other)
    {
        FindObjectOfType<DQArea>().checkPlayerDQMotion = null;

        Debug.Log("DQAREAA IS:" + GameManager.GetInstance().isDQ);
    }
    private void OnTriggerExit(Collider other)
    {
        FindObjectOfType<DQArea>().checkPlayerDQMotion = null;

        Debug.Log("DQAREAA EXSIT IS:" + GameManager.GetInstance().isDQ);
    }
    private void OnTriggerStay(Collider other)
    {
        checkPlayerDQMotion?.Invoke();

        //卸载事件
        if (checkPlayerDQMotion!=null)
        {
            checkPlayerDQMotion = null;
        }
    }
}
