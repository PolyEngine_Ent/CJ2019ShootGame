﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
using PolyEngine;


public class GunControllerPosition : MonoBehaviour
{

    [SerializeField]
    DynamicTarget m_Target;
    public enum RotationSource
    {
        IMU, Marker,
    }
    public RotationSource rotationSource = RotationSource.IMU;
    public TextMesh textMesh;

    // Update is called once per frame
    void LateUpdate()
    {
        if(Application.platform == RuntimePlatform.Android)
        {
            transform.position = m_Target.transform.position;
            if(rotationSource == RotationSource.Marker)
            {
                transform.rotation = m_Target.transform.rotation;
            }
            else
            {
                var ctrlRawQ = RXInput.GetControllerRotation();
                transform.localRotation = ctrlRawQ;
            }
           

            //textMesh.text = string.Format("IMU Q:{0}", ctrlRawQ.eulerAngles.PrettyAngle().ToString("F3"));
        }
    }
}
