﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
using Ximmerse.RhinoX.Internal;
using PolyEngine;
using PolyEngine.Reflection;
using System.Reflection;
using UnityEngine.Events;


public enum GunButtons
{
    Trigger = 0x40000,

    Function = 0x0002,
}

[System.Serializable]
public class GunButtonEvent : UnityEvent<GunButtons>
{

}

public class GunInput : MonoBehaviour
{
    public bool autoPair = true;
    public bool printButtonLog = true;
    public Transform markerT;
    public GunControllerRotation rotationComponent;

    public GunButtonEvent OnClickFunction = new GunButtonEvent();
    public GunButtonEvent OnClickTrigger = new GunButtonEvent();

    GunButtons[] allButtons = null;

    System.Type m_TypeXimmerseInputSystem;
    System.Type typeXimmerseInputSystem
    {
        get
        {
            if (m_TypeXimmerseInputSystem == null)
            {
                m_TypeXimmerseInputSystem = PEReflectionUtility.SearchTypeByName("Ximmerse.RhinoX.XimmerseControllerSystem");
            }
            return m_TypeXimmerseInputSystem;
        }
    }

    System.Object m_ControllerWrapper = null;

    System.Object ControllerWrapper
    {
        get
        {
            if (m_ControllerWrapper == null)
            {
                var ControllerWrapObj = typeXimmerseInputSystem.GetMethod("GetController", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
        .Invoke(null, new object[] { ControllerIndex.Any });

                if (ControllerWrapObj != null)
                {
                    m_ControllerWrapper = ControllerWrapObj;
                }
            }

            return m_ControllerWrapper;
        }
    }
    MethodInfo m_MethodGetControllerButtonState;
    MethodInfo methodGetControllerButtonState
    {
        get
        {
            if (m_MethodGetControllerButtonState == null)
            {
                if (ControllerWrapper != null)
                {
                    m_MethodGetControllerButtonState = ControllerWrapper.GetType().GetMethod("GetButtonState", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                }
            }
            return m_MethodGetControllerButtonState;
        }
    }

    MethodInfo m_MethodUnpairAll;
    MethodInfo methodUnpairAll
    {
        get
        {
            if (m_MethodUnpairAll == null)
            {
                m_MethodUnpairAll = typeXimmerseInputSystem.GetMethod("UnpairAll", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            }
            return m_MethodUnpairAll;
        }
    }


    MethodInfo m_MethodStartPairingByRFID;
    MethodInfo methodStartPairingByRFID
    {
        get
        {
            if (m_MethodStartPairingByRFID == null)
            {
                m_MethodStartPairingByRFID = typeXimmerseInputSystem.GetMethod("StartPairingByRFID", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            }
            return m_MethodStartPairingByRFID;
        }
    }

    FieldInfo field_IsTap = null;

    bool IsTap(Ximmerse.RhinoX.Internal.ControllerButton rawBtn)
    {
        if (ControllerWrapper != null && methodGetControllerButtonState != null)
        {
            var buttonState = methodGetControllerButtonState.Invoke(ControllerWrapper, new object[] { rawBtn });
            if (field_IsTap == null)
            {
                field_IsTap = buttonState.GetType().GetField("isTap", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            }
            bool isTap = (bool)field_IsTap.GetValue(buttonState);
            //Debug.LogFormat("IsTap : {0}, result: {1}", rawBtn, isTap);
            return isTap;
        }
        else
        {
            return false;
        }
    }

    private void Awake()
    {
        allButtons = new GunButtons[]
        {
             GunButtons.Trigger,
             GunButtons.Function,
        };
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        if (autoPair == false)
            yield break;
        Debug.Log("[AutoPairGun] Wait for AR begin.");
        while (ARCamera.Instance == null || ARCamera.Instance.IsARBegan == false)
            yield return null;
        yield return new WaitForSeconds(0.5f);

        UnpairAll();
        yield return new WaitForSeconds(1);

        StartPairingByRFID();
        yield return new WaitForSeconds(1);

    }

    private void LateUpdate()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (ControllerWrapper != null)
            {
                foreach (var btn in allButtons)
                {
                    if (IsTap((Ximmerse.RhinoX.Internal.ControllerButton)((int)btn)))
                    {
                        if(printButtonLog)
                           Debug.LogFormat("Tap on gun button : {0}", btn);

                        if(btn == GunButtons.Trigger)
                        {
                            OnClickTrigger.Invoke(btn);
                        }

                        if(btn == GunButtons.Function)
                        {
                            OnClickFunction.Invoke(btn);
                        }
                    }
                }
            }
        }
    }

    public void Recenter()
    {
        if(ControllerWrapper == null)
        {
            return;
        }
        var markerFwd = markerT.transform.forward;
        var ctrlRawQ = RXInput.GetControllerRotation();
        var methodTryRecenterCtrlRotation = ControllerWrapper.GetType().GetMethod("TryRecenterControllerRotation", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        var targetQ = Quaternion.LookRotation(markerFwd, ctrlRawQ * Vector3.up);
        var targetQ_Euler = targetQ.eulerAngles;
        targetQ_Euler.z = 0;
        targetQ.eulerAngles = targetQ_Euler;
        methodTryRecenterCtrlRotation.Invoke(ControllerWrapper, new object[] { targetQ });
    }


    public void UnpairAll ()
    {
        if (ARCamera.Instance == null || ARCamera.Instance.IsARBegan == false)
            return;
        methodUnpairAll.Invoke(null, null);
        Debug.Log("[AutoPairGun] Unpair all is invoked.");
    }


    public void StartPairingByRFID()
    {
        if (ARCamera.Instance == null || ARCamera.Instance.IsARBegan == false)
            return;
        methodStartPairingByRFID.Invoke(null, null);
        Debug.Log("[AutoPairGun] StartPairingByRFID is invoked.");
    }


    public void Quit()
    {
        Application.Quit();
    }
}



