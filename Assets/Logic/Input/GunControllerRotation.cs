﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
using PolyEngine;


public class GunControllerRotation : MonoBehaviour
{
    [SerializeField]
    float m_Gyro_PreYaw = 0;

    [SerializeField]
    float m_Gyro_PrePitch = 0, m_Gyro_PreRoll = 0;


    [SerializeField]
    Vector3 m_PostEuler = new Vector3(0, 0, 0);

    // Update is called once per frame
    void LateUpdate()
    {
        if(Application.platform == RuntimePlatform.Android)
        {
            var ctrlRawQ = RXInput.GetControllerRotation();
            Quaternion ctrlRotation = ctrlRawQ;
            Quaternion preYaw = Quaternion.AngleAxis(m_Gyro_PreYaw, Vector3.up);
            ctrlRotation = preYaw * ctrlRotation;

            Quaternion prePitch = Quaternion.AngleAxis(m_Gyro_PrePitch, Vector3.right);
            ctrlRotation = ctrlRotation * prePitch;

            Quaternion preRoll = Quaternion.AngleAxis(m_Gyro_PreRoll, Vector3.forward);
            ctrlRotation = ctrlRotation * preRoll;

            transform.localRotation = ctrlRotation * Quaternion.Euler (m_PostEuler);

        }
    }
}
