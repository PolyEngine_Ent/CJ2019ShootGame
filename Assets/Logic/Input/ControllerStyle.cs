﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PolyEngine;

public class ControllerStyle : MonoBehaviour
{
    [SerializeField] LineRenderer line;
    [SerializeField] GameObject sphere;
    // Start is called before the first frame update
    void Start()
    {
        sphere.transform.position = line.GetPosition(1);
    }


    RaycastHit hit;
    bool isSelected = false;
    // Update is called once per frame
    void Update()
    {

        line.SetPosition(0, line.transform.position);

        if (Physics.Raycast(line.GetPosition(0),line.transform.forward,out hit,LayerMask.GetMask("UI")))
        {
            sphere.transform.position = hit.point;
            line.SetPosition(1, hit.point);

            var stage = hit.collider.GetComponent<Stage>();

            if (isSelected && stage != null)
            {
                stage.EnterStage();
            }
        }
        else
        {
            line.SetPosition(1, line.transform.position + line.transform.forward * 10f);
            sphere.transform.position = line.GetPosition(1);
        }
    }
    [InspectFunction]
    public void TriggerDown()
    {
        isSelected = true;
        //sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
        Debug.Log(isSelected);
    }

    [InspectFunction]
    public void TriggerUp()
    {
        isSelected = false;
        //sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        Debug.Log(isSelected);
    }
}
