﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;
using Ximmerse.RhinoX.Internal;
using PolyEngine;

public enum InputMode
{
    Editor,
    RX
}
public class InputTest : MonoBehaviour
{
    public InputMode testMode;

    [SerializeField]
    GameObject arCamera;

    [SerializeField]
    GameObject ximController;

    [SerializeField]
    GameObject fpsController;

    public bool isTriggerTap;

    private static InputTest _instance;

    public static InputTest GetInstance()
    {
        if (_instance==null)
        {
            _instance = new InputTest();
        }
        return _instance;
    }

    private void Awake()
    {
        _instance = this;
        SwithInput();
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TriggerTracking());

        StartCoroutine(BluetoothConect());

        if (testMode ==InputMode.RX)
        {
            Debug.Log("这个是rx");
        }
    }

    IEnumerator TriggerTracking()
    {
        while (true)
        {
            isTriggerTap = GetTriggerTap();
            yield return 0;
        }
    }
    IEnumerator BluetoothConect()
    {
        yield return new WaitForSeconds(1f);
    }

    [InspectFunction]
    private bool GetTriggerTap()
    {
        bool isTriggerDown = false;

        switch (testMode)
        {
            case InputMode.Editor:
                isTriggerDown = Input.GetMouseButtonDown(0);
                //Debug.Log(isTriggerDown);
                break;
            case InputMode.RX:
                //GetComponent<RXController>().IsButtonDown(ControllerButtonCode.Trigger);
                var rxTriggerState = RXInput.IsButton(RhinoXButton.ControllerTrigger, ControllerIndex.Any);
                isTriggerDown = rxTriggerState;
                isTriggerDown = false;
                break;
            default:
                break;
        }
        return isTriggerDown;
    }

    [InspectFunction]
    void SwithInput()
    {
        switch (testMode)
        {
            case InputMode.Editor:
                //arCamera.SetActive(false);
                //ximController.SetActive(false);
                //fpsController.SetActive(true);

                fpsController = Instantiate(fpsController,Vector3.up*2f,Quaternion.identity);

                break;
            case InputMode.RX:
                //arCamera.SetActive(true);
                //ximController.SetActive(true);
                //fpsController.SetActive(false);


                arCamera = Instantiate(arCamera);
                ximController = Instantiate(ximController);
                break;
            default:
                break;
        }
    }
    IEnumerator VisibleGun()
    {
        yield return new WaitForSeconds(2f);
        
    }
}
