﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool
{
    List<GameObject> pool;
    List<GameObject> poolList;

    public void Push(GameObject go)
    {
        poolList.Add(go);
    }
    public GameObject Fetch(GameObject go)
    {
        if (poolList.Count==0)
        {
            var newGo = GameObject.Instantiate(go);
            poolList.Add(newGo);
            return newGo;
        }
        else
        {
            poolList.RemoveAt(0);
            return go;
        }
    }
}
