﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse.RhinoX;

public class InitRXEvent : MonoBehaviour
{
    [SerializeField] GameObject rxEvent;
    // Start is called before the first frame update
    void Start()
    {
        if (FindObjectOfType<RXEventSystem>()==null)
        {
           var go = Instantiate(rxEvent);
        }
    }

}
