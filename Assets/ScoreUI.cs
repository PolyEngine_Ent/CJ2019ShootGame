﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using PolyEngine;

public class ScoreUI : MonoBehaviour
{
    [SerializeField]
    public TextMeshProUGUI text;

    public static Action<Vector3, string> ShowScoreUI = null;

    private void Awake()
    {
        ShowScoreUI = Show;
    }

    private void OnEnable()
    {
        ShowScoreUI = Show;
    }

    private void OnDestroy()
    {
        ShowScoreUI = null;
    }

    [InspectFunction]
    void Show(Vector3 position,string str)
    {
        this.transform.position = position;
        text.text = str;
    }
}
